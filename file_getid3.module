<?php

/**
 * @file
 * Written by Henri MEDOT <henri.medot[AT]absyx[DOT]fr>
 * http://www.absyx.fr
 */

/**
 * Implements hook_field_info_alter().
 */
function file_getid3_field_info_alter(&$info) {
  $info['file']['instance_settings'] += array(
    'getid3' => 0,
  );
}

/**
 * Implements hook_form_FORM_ID_alter() for field_ui_field_edit_form().
 */
function file_getid3_form_field_ui_field_edit_form_alter(&$form, &$form_state, $form_id) {
  if ($form['#field']['type'] == 'file') {
    $settings = $form['#instance']['settings'];

    $additions['getid3'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable getID3()'),
      '#default_value' => isset($settings['getid3']) ? $settings['getid3'] : 0,
      '#description' => t('Analyze uploaded files with getID3() and attach the retrieved information to them.'),
    );

    $form['instance']['settings'] += $additions;
  }
}

/**
 * Implements hook_field_attach_insert().
 */
function file_getid3_field_attach_insert($entity_type, $entity) {
  list(, , $bundle) = entity_extract_ids($entity_type, $entity);

  foreach (field_info_instances($entity_type, $bundle) as $instance) {
    if (!empty($instance['settings']['getid3'])) {
      $field_name = $instance['field_name'];
      $field = field_info_field($field_name);
      $available_languages = field_available_languages($entity_type, $field);
      $languages = _field_language_suggestion($available_languages, NULL, $field_name);

      foreach ($languages as $langcode) {
        $items = isset($entity->{$field_name}[$langcode]) ? $entity->{$field_name}[$langcode] : array();
        foreach ($items as $item) {
          $file = (object) $item;
          $file->uri = file_load($file->fid)->uri;
          $file->getid3_info = _file_getid3_analyze($file->uri);
          _file_getid3_file_save($file);
        }
      }
    }
  }
}

/**
 * Implements hook_field_attach_update().
 */
function file_getid3_field_attach_update($entity_type, $entity) {
  file_getid3_field_attach_insert($entity_type, $entity);
}

/**
 * Implements hook_file_insert().
 */
function file_getid3_file_insert($file) {
  _file_getid3_file_save($file);
}

/**
 * Implements hook_file_update().
 */
function file_getid3_file_update($file) {
  _file_getid3_file_save($file);
}

/**
 * Implements hook_file_delete().
 */
function file_getid3_file_delete($file) {
  _file_getid3_file_delete($file);
}

/**
 * Implements hook_query_TAG_alter() for queries tagged with file_load_multiple.
 */
function file_getid3_query_file_load_multiple_alter(QueryAlterableInterface $query) {
  $query->addField('fg', 'getid3_info');
  $query->leftJoin('file_getid3', 'fg', 'fg.fid = base.fid');
}

/**
 * Implements hook_file_load().
 */
function file_getid3_file_load($files) {
  foreach ($files as $file) {
    if (isset($file->getid3_info)) {
      $file->getid3_info = unserialize($file->getid3_info);
    }
  }
}

/**
 * Save getID3() info.
 */
function _file_getid3_file_save($file) {
  if (empty($file->getid3_info)) {
    _file_getid3_file_delete($file);
  }
  else {
    db_merge('file_getid3')
      ->key(array('fid' => $file->fid))
      ->fields(array('getid3_info' => serialize($file->getid3_info)))
      ->execute();
  }
}

/**
 * Delete getID3() info.
 */
function _file_getid3_file_delete($file) {
  return db_delete('file_getid3')->condition('fid', $file->fid)->execute();
}

/**
 * Analyze a file with getID3().
 */
function _file_getid3_analyze($uri) {
  if ($path = drupal_realpath($uri)) {
    $info = getid3_analyze($path);
    if ($info && empty($info['error'])) {
      return $info;
    }
  }
}
