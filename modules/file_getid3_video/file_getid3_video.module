<?php

/**
 * @file
 * Written by Henri MEDOT <henri.medot[AT]absyx[DOT]fr>
 * http://www.absyx.fr
 */

/**
 * Implements hook_field_formatter_info().
 */
function file_getid3_video_field_formatter_info() {
  $formatters = array(
    'file_getid3_video' => array(
      'label' => t('File getID3() HTML5 video player'),
      'field types' => array('file'),
      'settings' => array('poster_field' => '', 'poster_style' => ''),
    ),
  );

  return $formatters;
}

/**
 * Implements hook_field_formatter_settings_form().
 */
function file_getid3_video_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];

  $image_fields = file_getid3_video_image_field_options($instance['entity_type'], $instance['bundle']);
  $element['poster_field'] = array(
    '#title' => t('Poster field'),
    '#type' => 'select',
    '#default_value' => $settings['poster_field'],
    '#empty_option' => t('- None -'),
    '#options' => $image_fields,
    '#description' => t('Use the first image from this field as the video poster.'),
  );

  $image_styles = image_style_options(FALSE, PASS_THROUGH);
  $element['poster_style'] = array(
    '#title' => t('Poster style'),
    '#type' => 'select',
    '#default_value' => $settings['poster_style'],
    '#empty_option' => t('None (original image)'),
    '#options' => $image_styles,
  );

  return $element;
}

/**
 * Implements hook_field_formatter_settings_summary().
 */
function file_getid3_video_field_formatter_settings_summary($field, $instance, $view_mode) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];
  $summary = array();

  $image_fields = file_getid3_video_image_field_options($instance['entity_type'], $instance['bundle']);
  unset($image_fields['']);
  $has_poster_field = isset($image_fields[$settings['poster_field']]);
  $summary[] = t('Poster field: @field', array('@field' => $has_poster_field ? $image_fields[$settings['poster_field']] : t('None')));

  if ($has_poster_field) {
    $image_styles = image_style_options(FALSE, PASS_THROUGH);
    unset($image_styles['']);
    $summary[] = t('Poster style: @style', array('@style' => isset($image_styles[$settings['poster_style']]) ? $image_styles[$settings['poster_style']] : t('Original image')));
  }

  return implode('<br />', $summary);
}

/**
 * Implements hook_field_formatter_view().
 */
function file_getid3_video_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $settings = $display['settings'];

  list($id, , ) = entity_extract_ids($entity_type, $entity);
  $element[0] = array(
    '#theme' => 'file_getid3_video',
    '#attached' => array('css' => array(drupal_get_path('module', 'file_getid3_video') .'/file_getid3_video.css')),
    '#items' => $items,
    '#poster_style' => $settings['poster_style'],
    '#video_id' => implode('-', array('file-getid3-video', $entity_type, $id, $field['field_name'])),
  );

  if (!empty($settings['poster_field']) && ($poster_items = field_get_items($entity_type, $entity, $settings['poster_field'], $langcode))) {
    $element[0]['#poster_item'] = reset($poster_items);
  }

  return $element;
}

/**
 * Implements hook_theme().
 */
function file_getid3_video_theme() {
  return array(
    'file_getid3_video' => array(
      'variables' => array('items' => NULL, 'poster_item' => NULL, 'poster_style' => NULL, 'video_id' => NULL),
      'template' => 'file-getid3-video',
    ),
  );
}

/**
 * Implements template_preprocess_HOOK for file-getid3-video.tpl.php
 */
function template_preprocess_file_getid3_video(&$variables) {
  $variables['video_classes_array'] = array();
  $variables['video_attributes_array'] = array();

  if ($variables['items']) {
    $width = 640;
    $height = 360;
    $item = reset($variables['items']);
    if (!empty($item['getid3_info'])) {
      $info = $item['getid3_info'];
      if (isset($info['video']['resolution_x'], $info['video']['resolution_y'])) {
        $width = intval($info['video']['resolution_x']);
        $height = intval($info['video']['resolution_y']);
      }
    }
    $variables['attributes_array']['style'] = 'max-width:'. $width .'px;';
    $variables['padding_bottom'] = (100 * $height / $width) .'%';

    $variables['video_attributes_array'] = array(
      'controls' => 'controls',
      'preload' => 'auto',
    );
    if ($variables['poster_item']) {
      $variables['video_attributes_array']['poster'] = $variables['poster_style'] ? image_style_url($variables['poster_style'], $variables['poster_item']['uri']) : file_create_url($variables['poster_item']['uri']);
    }
  }
}

/**
 * Implements template_process_HOOK for file-getid3-video.tpl.php
 */
function template_process_file_getid3_video(&$variables) {
  $variables['video_classes'] = implode(' ', $variables['video_classes_array']);
  $variables['video_attributes'] = $variables['video_attributes_array'] ? drupal_attributes($variables['video_attributes_array']) : '';
}

/**
 * Gets an array of image fields suitable for using as select list options.
 */
function file_getid3_video_image_field_options($entity_type, $bundle) {
  $instances = field_info_instances($entity_type, $bundle);
  $options = array();

  foreach ($instances as $instance) {
    $field = field_info_field($instance['field_name']);
    if ($field['type'] == 'image') {
      $options[$instance['field_name']] = $instance['label'];
    }
  }
  if (empty($options)) {
    $options[''] = t('No available image fields');
  }

  return $options;
}
